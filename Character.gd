extends KinematicBody

var gravity = 9.8
var velocity = Vector3()
var camera
var anim_player
var character
var direction = Vector3()
var hv = Vector3()
var movespeed = 200
var lastPos = Vector3()
const IGNORE_VALUE = 0.01

const SPEED = 6

onready var joystick = get_parent().get_node("Control/JoyStick/JoyStickButton")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	anim_player = get_node("AnimationPlayer")
	character = get_node(".")
	
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _physics_process(delta):
	virtual_joystick()
	direction = direction.normalized()
	direction = direction * movespeed * delta
	velocity.y -= gravity * delta
	velocity.x = direction.x
	velocity.z = direction.z
	var t = transform
	var speed = Vector2(t.origin.x,t.origin.z).distance_to(Vector2(lastPos.x,lastPos.z))
	var rotTransform
	var thisRotation

	if (speed >= IGNORE_VALUE):
		rotTransform = t.looking_at(lastPos,Vector3(0,1,0)) 
		thisRotation = Quat(t.basis.orthonormalized()).slerp(rotTransform.basis.orthonormalized(), 0.1)
		transform = Transform(thisRotation,t.origin)
	
	lastPos = t.origin
	rotation.x = 0
	rotation.z = 0

	velocity = move_and_slide(velocity,Vector3.UP)
	
	speed = direction.length() / SPEED
	
	get_node("AnimationTreePlayer").blend2_node_set_amount("Idle_Run", speed)
	
func virtual_joystick():
	direction = Vector3(0,0,0)
	var dir_analog = Vector2(joystick.get_value().x, joystick.get_value().y)
	var cam_rot_y = get_node("target/Camera").get_global_transform().basis
	if dir_analog.length() > 0:
		direction += dir_analog.x * cam_rot_y.x
		direction += dir_analog.y * cam_rot_y.z
		
	direction = direction.normalized()


